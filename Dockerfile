FROM ubuntu:22.04

ENV LC_ALL=C.UTF-8 LANG=C.UTF-8

# Created with:
#   wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor > hashicorp.gpg
# See https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli
COPY hashicorp.gpg /usr/share/keyrings/

RUN --mount=type=cache,target=/var/cache/apt,sharing=locked,id=apt \
    apt-get update \
    && apt-get install -y \
        ca-certificates \
        gpg \
        lsb-release \
    && echo "deb [signed-by=/usr/share/keyrings/hashicorp.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" > /etc/apt/sources.list.d/hashicorp.list \
    && apt-get update \
    && apt-get install -y \
        curl \
        git-lfs \
        jq \
        postgresql-client \
        terraform \
        unzip \
        wget \
    && rm -rf /var/lib/apt/lists/*

CMD ["sleep", "7d"]
