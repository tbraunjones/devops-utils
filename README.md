# devops-utils

Handy container image for doing debugging in a Kubernetes pod, for example.

## Build

```sh
podman build . -t devops-utils
```

## Publish

```sh
podman push devops-utils docker://docker.io/tayloratcyto/devops-utils:latest
```
